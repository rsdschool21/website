.. _day2:

Step 2 - Injecting load
=======================

**Objective**
  - Simple load injection using home-made python script.
  - First analysis using R
  - Graph generation with ggplot

**Notebooks**
  - ``analysis/03-grid5000/03-g5k-iotlab.ipynb``


What to do?
-----------

1. Open notebook: ``analysis/03-grid5000/03-g5k-iotlab.ipynb``
2. Inspect the code

   1. How the load-injection is done?
   2. Which parameters are used throughout the notebook? How and where are they configured?
   3. What are the differences compared to the first one (00-ping-test)?

3. Run the notebook
   
   1. Did it work?
   2. Where the results are saved?
   3. Give a look at the generated artefacts.

   Alternatively, you may want to redo the analysis part of the existing experiment. For that:

   1. Run the first cell to adjust the current path
   2. Uncomment the cell just below "Analyze data" subsection.

   You may to redo and improve the initial data analysis.
