.. _day5:

Step 5 - OS's & Energy
======================

In all experiments up to now, we have a single variable (:math:`\lambda`) and study a single metric (response time).

In this step, we add another variable to the experiment, studying the impact of the M3 firmware on the energy consumption and response time.

Here, we vary:

- :math:`\lambda`: different number of clients injecting load
- **OS**: contikimac, tsch and nullrdc

**Objective**
  - Compare energy consumption of 3 different firmwares
  - Compare response time

**Notebook**
  - ``analysis/08-energy-locust/08-energy-locust.ipynb``: base notebook
  - ``analysis/08-energy-locust/2021-09-29-energy_locust-exp1/``: example


What to do?
-----------

1. Open notebook: ``analysis/08-energy-locust/08-energy-locust.ipynb``

2. Run the experiment (please follow the steps detailed in :ref:`day3_run_exp`)
   
   1. Check what we had to change to study the firmware in this notebook.
   2. By default, this test takes a long time (more than 2h). You may want to do less tests to be faster or change the reservation period for each testbed. Or yet, run it in several times.
   3. Meanwhile, see the example: ``analysis/08-energy-locust/2021-09-29-energy_locust-exp1/``

3. Analysis

   1. Compare the energy consumption among the 3 firmwares (contikimac, nullrdc, tsch). Are they different?
   2. Does the :math:`\lambda` impacts on the energy consumption?
   3. What about the response time? Does the firmware have an impact?
   4. Do you have the same behavior of our example in your tests?
   5. How would you improve this study?

4. Extra (optional)
   
   1. You can compare with some results in ``analysis/07-energy/`` which uses the python load injector instead of locust.
   2. Perform other study. Different workloads? Other M3 sensors? Change experiment duration?
