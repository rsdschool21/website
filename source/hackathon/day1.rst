.. _day1:

Step 1 - EnOSlib starter
========================

**Objective**
  - Scripting an experiment
  - Introduction to EnOSlib

**Notebooks**
  - ``analysis/00-ping-test/00-ping-test.ipynb``

Gentle introduction to EnOSlib, implement the hands-on in a notebook using EnOSlib.


What to do?
-----------

1. Open notebook: ``analysis/00-ping-test/00-ping-test.ipynb``
2. Inspect the code

   - Can you identify the commands you executed by hand?
   - What about choosing the resources in Grid'5000 and IoT-LAB?
   - Identify the things you may want to change during experiences, e.g.: firmware, ipv6 address, etc.
3. Run the notebook

   - Do you have the same results?
   - Pay attention to the ansible's output for the commands. You may want to print the raw result from *"en.run"* command to understand what it has inside.
