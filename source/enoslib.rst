.. _enoslib_tutorial:

|enoslib| Tutorial
===================

The tutorial will use the notebooks available in the lab environment repository.
So you'll need to start a ``jupyter lab`` environment (see :ref:`run_container`)

There are two types of tutorials:

- The |enoslib|'s specific tutorials that are available in the
  ``enoslib`` subdirectory of the lab environment.

- A tutorial which implements a basic experiment spanning both
  Grid'5000 and FIT platform.

.. list-table:: Grid'5000 tutorials
   :header-rows: 1

   * - File
     - Goal

   * - ``enoslib/00_setup_and_basics.ipynb``
     - Discover the base data structures in use in |enoslib|
   * - ``enoslib/01_remote_actions_and_variables.ipynb``
     - Run your first remote commands, work with variables
   * - ``enoslib/02_observability.ipynb``
     - Discover some built-in observability services
   * - ``enoslib/03_using_several_networks.ipynb``
     - Use several networks (e.g kavlan)
   * - ``enoslib/04_working_with_virtualized_resources.ipynb``
     - Provision virtualized resources on Grid'5000 with some alternative
       providers
   * - ``enoslib/05_DIY.ipynb``
     - 🎈 Freestyle 🎈

.. list-table:: Grid'5000 + FIT tutorials
   :header-rows: 1

   * - File
     - Goal

   * - ``analysis/00-ping-test/00-ping-test.ipynb``
     - Implement you first experiment spanning Grid'5000 and FIT.