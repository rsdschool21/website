https://rsd-ecole.cnrs.fr/prog/

Agenda
======

.. tabs::

    .. tab:: Monday

        - **Introduction** (`slides <_static/slides/school_introduction.pdf>`_)
	- **Keynote** - Olivier Bonaventure (`slides <_static/slides/keynote_olivier_bonaventure.pdf>`_)
        - **Lecture: FIT-IoT lab et Grid5000** - Simon Delamare, Guillaume Schreiner ( `slides <_static/slides/iotlab_g5k.pdf>`_)
        - **Hackaton #0** presentation and first contact with the platforms
            - :ref:`day0`

    .. tab:: Tuesday

        - **Lecture** |enoslib| - Bruno Donassolo, Matthieu Simonin (`slides <_static/slides/rescom_enoslib_msimonin.pdf>`_)

        - **Tutorial** |enoslib| - Bruno Donassolo, Matthieu Simonin
            - :ref:`enoslib_tutorial`

          A Walkthough EnOSlib's features on Grid'5000:

            - Manipulating the basic objects (Host, Network, Roles)
            - Acting on remote resources (run commands, pipeline of actions)
            - Discovering some of the observability tools shipped with EnOSlib
            - Working with several networks
            - Mixing the compute resource types (virtual machines, containers)

          All the resources are located in the *enoslib/* subdirectory of the hackathon repository.

        - **Round Table**: Platform and load models
	- **Keynote** - Philippe Bonnet (`slides <_static/slides/keynote_philippe_bonnet.pdf>`_)
        - **Round Table**: Observation and Tracing
        - **Hackaton #1**: Running experiments
            - :ref:`day2`
            - :ref:`day3` (beginning)

    .. tab:: Wednesday

        - **Lecture: Data analysis** - Arnaud Legrand
            - Avoid uggly graphics (`page 13 <https://github.com/alegrand/SMPE/raw/master/lectures/lecture_data_presentation.pdf>`_)
            - `Introduction to R and the tidyverse (dplyr, ggplot2) <https://github.com/alegrand/SMPE/raw/master/lectures/lecture_R_crash_course.pdf>`_ to summarize and visualize a series of measurements.
            - Central Limit Theorem, confidence interval, and
	      important hypothesis (`pages 1-49 <https://github.com/alegrand/SMPE/raw/master/lectures/3_introduction_to_statistics.pdf>`_)
            - Dependent variables, Linear regression, and important hypothesis (`pages 1-41 <https://github.com/alegrand/SMPE/raw/master/lectures/4_linear_model.pdf>`_)

       - **Tutorial: Data analysis** - Arnaud Legrand
          `Synthetic data sets <_static/lecture-data-analysis/datasets.tgz>`_
            - Curation and graphical verification
            - Summarizing data
            - Linear regression (modeling and parameter evaluation)


        - **Best PhD prize GDR-RSD**: Ahmed Boubrima

        - **Lecture: Design of Experiments** - Arnaud Legrand (`slides <https://github.com/alegrand/SMPE/raw/master/lectures/5_design_of_experiments.pdf>`_)
            - Randomizing inputs to avoid bias and sequences to avoid temporal bias (`pages 5-19 <https://github.com/alegrand/SMPE/raw/master/lectures/talk_20_01_23_Nantes_RSD.pdf>`_ and `pages 50-63 <https://github.com/alegrand/SMPE/raw/master/lectures/3_introduction_to_statistics.pdf>`_)
            - Parameter identification and experimental workflow
            - Deciding input parameters: what for ? (screening, model design,
              pameter selection, optimization, etc.)

        - **Hackaton #2:** Now, it's your turn!

          Evaluating stability/reproducibility for:
            - :ref:`day3` (contd)
            - :ref:`day5`

          Possibly update the experiment design.

    .. tab:: Thursday

        - **Tutorial: Design of experiments** - Arnaud Legrand

          "Experimental" functions would be provided through a shiny app

            - Generating simple designs.
            - Identifying parameters

        - **Round Table:** Simulation/Emulation/Experimentation

        - **Hackaton #3:** Start comparing between groups

    .. tab:: Friday

        - **Lecture: Archive, identication, description and citation of source code for research software** - Morane Gruenpeter (`slides <_static/slides/swh_mgruenpeter.pdf>`_)
        - **Hackaton #4:** That's all folks

          Discussion on the following topics:

            - Reproducibility, at what cost ?
            - Representativity ?
            - ...
