.. RSD Research Autumn School 2021 documentation master file, created by
   sphinx-quickstart on Wed Aug 18 14:36:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

============================================
 Welcome to RSD Research Autumn School 2021
============================================

Let's use Mattermost_ to discuss and CodiMD_ for collaborative
notes.

.. _Mattermost: https://framateam.org/fcsr/channels/rsd-school-reproducible-experiments
.. _CodiMD: https://notes.inria.fr/ksoGb5VUTruStWvzujOb7A#

.. toctree::
    :maxdepth: 1

    starting.rst
    agenda.rst
    hackathon/index.rst
    enoslib.rst
    references.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
