Useful References
=================

EnOSlib tutorials
-----------------

https://discovery.gitlabpages.inria.fr/enoslib/tutorials.html

Grid 5000: Getting Started
--------------------------

https://www.grid5000.fr/w/Getting_Started

FIT IoT-Lab: Getting Started
----------------------------

https://www.iot-lab.info/legacy/tutorials/getting-started-tutorial/index.html

